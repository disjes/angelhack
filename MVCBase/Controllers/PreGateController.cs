﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.VisualBasic;

namespace MVCBase.Controllers
{
     [Authorize(Roles = "pregate")]
    public class PreGateController : Controller
    {
        //
        // GET: /PreGate/        

        PregateEntities db = new PregateEntities();

        public ActionResult Index()
        {
            ViewBag.seccion = "PreGate";
            ViewBag.view = "Busqueda";
            ViewBag.index = true;
            return View();
        }

        [HttpPost]
        public ActionResult PartialInfo(string id)
        {
            ViewBag.seccion = "PreGate";
            ViewBag.view = "Busqueda";
            if(!string.IsNullOrEmpty(id))
            {
                if (!char.IsNumber(id.Substring(0, 1)[0])) return PartialUnit(new SrvN4Ref.N4Client().GetUnit("svcGATE", "gate123", id));
                else return PartialAppointment(new SrvN4Ref.N4Client().GetAppt("svcGATE", "gate123", id));
            }
            ViewBag.approved = false;
            ViewBag.mensaje = "DEBE INGRESAR UN UNIT / APPOINTMENT VALIDO"; 
            return View("Index");
        }

        public ActionResult PartialAppointment(SrvN4Ref.vwAppts appt)
        {
            ViewBag.index = false;
            ViewBag.seccion = "PreGate";
            ViewBag.view = "Busqueda";
            if (appt != null)
            {
                if (appt.state == "USED") { ViewBag.approved = false; ViewBag.mensaje = "NO APROBADO: USED"; }
                else if (appt.state == "USEDLATE") { ViewBag.approved = false; ViewBag.mensaje = "NO APROBADO: USEDLATE"; }
                else if (appt.state == "EXPIRED") { ViewBag.approved = false; ViewBag.mensaje = "NO APROBADO: EXPIRED"; }
                else if (appt.state == "CANCEL") { ViewBag.approved = false; ViewBag.mensaje = "NO APROBADO: CANCEL"; }
                else { if (appt.state == "CREATED" && appt.onTime == 1) { ViewBag.approved = true; ViewBag.mensaje = "APROBADO"; } else { ViewBag.approved = false; ViewBag.mensaje = "LLEGADA NO VALIDA: FUERA DE HORARIO ESTABLECIDO O NO APROBADO"; } }
                
                ViewBag.mensaje += " " + appt.id.ToString();
                db.GatePass.Add(new GatePass() { estado = appt.state, fecha = DateTime.Now, n4Appointment = appt.id, transType = appt.trans_type, usuario = User.Identity.Name, onTime = Convert.ToBoolean(ViewBag.approved)});
                db.SaveChanges();
                return View("Index");
            }
            ViewBag.approved = false;
            ViewBag.mensaje = "NO APROBADO " + appt.id.ToString();; 
            return View("Index");
        }

        public ActionResult PartialUnit(SrvN4Ref.vwUnits unit)
        {
            string state = "";
            ViewBag.index = false;
            ViewBag.seccion = "PreGate";
            ViewBag.view = "Busqueda";
            if(unit != null)
            {
                if (unit.late_unit != null) { ViewBag.approved = CompareDates(DateTime.Now, Convert.ToDateTime(unit.late_cutoff)); state = "lateunit"; }
                else if (unit.hazardous != null && unit.hazardous != false) { ViewBag.approved = CompareDates(DateTime.Now, Convert.ToDateTime(unit.UfvFlexDate01)); state = "hazardous"; }
                else if (unit.Reefer != null && unit.Reefer != false) { ViewBag.approved = CompareDates(DateTime.Now, Convert.ToDateTime(unit.UfvFlexDate03)); state = "reefer"; }
                else { ViewBag.approved = CompareDates(DateTime.Now, Convert.ToDateTime(unit.UfvFlexDate02)); state = "dry"; }
                db.PreAdvised.Add(new PreAdvised() { estado = state, fecha = DateTime.Now, usuario = User.Identity.Name, idunit = unit.idunit, onTime = Convert.ToBoolean(ViewBag.approved), unitNbr = unit.UnitNbr});
                ViewBag.mensaje += " " + unit.UnitNbr;
                db.SaveChanges();
                return View("Index");
            }
            ViewBag.approved = false;
            ViewBag.mensaje =  "NO PREAVISADO "; 
            return View("Index");
        }

        private bool CompareDates(DateTime firstDate, DateTime secondDate)
        {            
            if(firstDate <= secondDate) { ViewBag.mensaje = "APROBADO"; return true; }
            ViewBag.mensaje = "LLEGADA NO VALIDA: FUERA DE HORARIO ESTABLECIDO O NO APROBADO";
            return false;
        }

        //
        // GET: /PreGate/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /PreGate/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /PreGate/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /PreGate/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /PreGate/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /PreGate/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /PreGate/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
