//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MVCBase
{
    using System;
    using System.Collections.Generic;
    
    public partial class PreAdvised
    {
        public int idPreAdvised { get; set; }
        public Nullable<System.DateTime> fecha { get; set; }
        public Nullable<long> idunit { get; set; }
        public string unitNbr { get; set; }
        public string estado { get; set; }
        public string usuario { get; set; }
        public Nullable<bool> onTime { get; set; }
    }
}
